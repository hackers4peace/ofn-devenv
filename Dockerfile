FROM ruby:2.1.5

COPY openfoodnetwork /openfoodnetwork
WORKDIR /openfoodnetwork
RUN bundle install
