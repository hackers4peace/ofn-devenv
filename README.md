# OFN development environment

### OSX

see branch: https://gitlab.com/hackers4peace/ofn-devenv/tree/osx

### usage
```shell
# on host
git submodule init && git submodule update
cp openfoodnetwork/config/application.yml.example openfoodnetwork/config/application.yml
docker-compose run ofn bash
# in container
bundle exec rake db:setup
bundle exec rake openfoodnetwork:dev:load_sample_data
exit
# on host
docker-compose up
```

